# Programación declarativa y funcional

## Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  Linecolor #FF4300
}
</style>

*[#044E51] <font color=#FFFFFF>Programación Declarativa. \n<font color=#FFFFFF>Orientaciones y pautas \n<font color=#FFFFFF>para el estudio

	*_ es
		*[#079CA1] <font color=#FFFFFF>Un paradigma de programación
			*_ es
				*[#36C5FF] Conocido
					*_ como
						*[#FFFFFF] Programación perezosa.
						*[#FFFFFF] Programación comoda.
		*[#079CA1] <font color=#FFFFFF>Un estilo de construcción
			*_ de
				*[#36C5FF] la estructura.
				*[#36C5FF] los elementos.

	*_ existen
		*[#079CA1] <font color=#FFFFFF>Tipos de programación
			*_ la
				*[#36C5FF] Creativa
					*[#FFFFFF] Se basa en ideas.
					*[#FFFFFF] Busca ahorrar memoria.
					*[#FFFFFF] Tiempos cortos.
				*[#36C5FF] Burocrática
					*[#FFFFFF] Gestión detallada de la memoria.
					*[#FFFFFF] Secuencia de órdenes.

	*_ sus
		*[#079CA1] <font color=#FFFFFF>Características
			*_ hace
				*[#36C5FF] Énfasis en aspectos creativos
			*_ las
				*[#36C5FF] Tareas rutinarias de programación se dejan al compilador.
			*_ surge
				*[#36C5FF] Como reacción a problemas de la programación clásica.
			*_ su
				*[#36C5FF] Proposito
					*_ es reducir
						*[#FFFFFF] La complejidad de los programas.
						*[#FFFFFF] El riesgo de cometer errores.

	*_ se
		*[#079CA1] <font color=#FFFFFF>Enfoca
			*_ en
				*[#36C5FF] Liberarse
					*_ de 
						*[#FFFFFF] Las asignaciones.
						*[#FFFFFF] Detallar específicamente el control de la gestión de memoria.
				*[#FFFFFF] Utilizar recursos expresivos.
				*[#FFFFFF] Asemejar los programas a la forma de pensar del programador.
				*[#36C5FF] Ventajas
					*_ el
						*[#FFFFFF] Tiempo de desarrollo es más rápido.
						*[#FFFFFF] Tiempo  de depuración es rápido.
						*[#FFFFFF] Obtener programas cortos.

	*_ sus
		*[#079CA1] <font color=#FFFFFF>Variantes
			*_ son
				*[#36C5FF] Funcional
					*_ emplea
						*[#FFFFFF] El lenguaje de los matemáticos.
					*_ se basa
						*[#FFFFFF] Razonamiento ecuacional
							*_ donde
								*[#FFFFFF] Una función se asimila a un programa
									*_ recibe 
										*[#FFFFFF] Datos de entrada
											*[#FFFFFF] Argumentos.
									*_ devuelve
										*[#FFFFFF] Datos de salida
											*[#FFFFFF] Resultado de una función.
					*_ sus
						*[#FFFFFF] Características
							*_ son
								*[#FFFFFF] Describe funciones.
								*[#FFFFFF] Capacidad expresiva.
								*[#FFFFFF] Suceciones infinitas.
							*_ utiliza
								*[#FFFFFF] Evaluación perezosa
									*_ solo
										*[#FFFFFF] Calcula aquello que es estrictamente necesario. 
					*_ sus
						*[#FFFFFF] Ventajas
							*[#FFFFFF] Se pueden usar funciones de orden superior.
							*[#FFFFFF] Más potentes.

				*[#36C5FF] Lógica
					*_ recurre
						*[#FFFFFF] A la lógica de predicados de primer orden.
					*_ establece
						*[#FFFFFF] Relación entre objetos.
					*_ sus
						*[#FFFFFF] Característica
							*_ son
								*[#FFFFFF] No estable orden entre los argumentos.
								*[#FFFFFF] Parte de hechos o reglas.
								*[#FFFFFF] Hace inferencias.
						*[#FFFFFF] Ventajas
							*_ son
								*[#FFFFFF] Permite ser más declarativo.
								*[#FFFFFF] Busca posibles soluciones.
					*_ es posible
						*[#FFFFFF] Orientarlo a la \ninteligencia artificial
							*_ usa
								*[#FFFFFF] El lenguaje HASKELL
									*_ es
										*[#FFFFFF] El estándar
									*_ sus 
										*[#FFFFFF] Compiladores
											*_ son
												*[#FFFFFF] Gofer
												*[#FFFFFF] Hugs
@endmindmap
```


Recursos utilizados

[Video 1 - Programación Declarativa. Orientaciones](https://canal.uned.es/video/5a6f2c66b1111f54718b4911)

[Video 2 - Programación Declarativa. Orientaciones y pautas para el estudio](https://canal.uned.es/video/5a6f2c5bb1111f54718b488b)

[Video 3 - Programación Declarativa. Orientaciones](https://canal.uned.es/video/5a6f2c42b1111f54718b4757)

## Lenguaje de Programación Funcional (2015)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  Linecolor #FF4300
}
</style>

*[#044E51] <font color=#FFFFFF>Programación Funcional

	*_ sus
		*[#079CA1] <font color=#FFFFFF>Antecedentes 
			*_ los
				*[#36C5FF] Programas
					*_ basados en
						*[#FFFFFF] El modelo de Von Neumann
							*_ en honor 
								*[#FFFFFF] Al matemático y físico 
									*[#FFFFFF] John von Neumann.
								*_ en
									*[#FFFFFF] 1945.
							*_ propuso que
								*[#FFFFFF] Los programas debían almacenarse en la misma máquina antes de ser ejecutados.
								*[#FFFFFF] La ejecución consistiría en una serie de instrucciones que se ejecutarán secuencialmente. 
			*_ sus
				*[#36C5FF] Secuencias
					*_ se
						*[#FFFFFF] Podían modificar según el estado del cómputo.
					*_ es
						*[#FFFFFF] Una zona de memoria
							*_ se
								*[#FFFFFF] Puede acceder con una serie de variables.
							*_ en algún momento
								*[#FFFFFF] Almacena el valor de todas las variables.
			*_ las
				*[#36C5FF] Variables
					*_ están
						*[#FFFFFF] Definidas en el programa.
					*_ pueden
						*[#FFFFFF] Modificarse mediante instrucciones de asignación.
					*_ conservan
						*[#FFFFFF] El valor hasta que no se le indique lo contrario.
			*_ el
				*[#36C5FF] Paradigma
					*_ es un
						*[#FFFFFF] Modelo de computación
							*_ incluye
								*[#FFFFFF] Lenguajes
									*_ dotan
										*[#FFFFFF] De semántica a los programas.
									*_ contienen
										*[#FFFFFF] Interpretación de las sentencias.
					*_ el
						*[#FFFFFF] Imperativo
							*_ sus
								*[#FFFFFF] Ventajas
									*_ son
										*[#FFFFFF] Fácilmente legible.
										*[#FFFFFF] Fácil de aprender en lo relativo a comportamientos.
								*[#FFFFFF] Desventajas
									*_ son
										*[#FFFFFF] La optimización y la ampliación son más difíciles.
										*[#FFFFFF] Mayor riesgo de cometer errores.
					*_ tiene
						*[#FFFFFF] Variantes
							*_ una de ellas
								*[#FFFFFF] Programación orientada a objetos
									*_ son
										*[#FFFFFF] Pequeños trozos de código.
										*[#FFFFFF] Objetos que interactúan entre sí.
									*_ se
										*[#FFFFFF] Compone de instrucciones.
										*[#FFFFFF] Ejecuta secuencialmente.
							*_ la
								*[#FFFFFF] Lógica simbólica
									*_ es
										*[#FFFFFF] Paradigma de programación lógico
											*_ su
												*[#FFFFFF] Programa
													*_ es
														*[#FFFFFF] Formado por un conjunto de secuencias.
														*[#FFFFFF] Define veracidad.
											*_ utiliza
												*[#FFFFFF] Inferencia lógica
													*_ para
														*[#FFFFFF] Para resolver el problema.
									*_ no
										*[#FFFFFF] Necesita que se le indique los pasos a realizar.
										*_ recibe
											*[#FFFFFF] Datos
										*_ devuelve
											*[#FFFFFF] Definiciones.
	
	*_ las
		*[#079CA1] <font color=#FFFFFF>Ventajas
			*_ son
				*[#FFFFFF] Escribir código más rápido
				*[#FFFFFF] La transparencia referencial 
					*_ los
						*[#FFFFFF] Programas
							*_ solo
								*[#FFFFFF] Dependen de los parámetros de entrada.
							*_ son
								*[#FFFFFF] Independientes del orden en el que se realicen los cálculos.
				*[#FFFFFF] El orden no es relevante.
				*[#FFFFFF] Paralizar las funciones
					*_ en
						*[#FFFFFF] El entorno multiprocesado.
					*_ no existe
						*[#FFFFFF] Dependencia entre funciones.
				*[#FFFFFF] Las funciones son ciudadanos de primera clase.
					*_ poseen
						*[#FFFFFF] La capacidad de estar en cualquier parte del programa.
				*[#FFFFFF] Currificación
					*_ es
						*[#FFFFFF] Una función que 
							*_ recibe
								*[#FFFFFF] Varias funciones de entradda.
							*_ devuelve
								*[#FFFFFF] Diferentes salidad.
					*_ conocido
						*[#FFFFFF] Aplicación parcial.
					*_ en honor
						*[#FFFFFF] Haskell curry.

	*_ sus
		*[#079CA1] <font color=#FFFFFF>Consecuencias
			*_ en
				*[#36C5FF] las variables
					*_ se pierde
						*[#FFFFFF] El concepto
							*_ como
								*[#FFFFFF] Zona de memoria que se puede modificar.
					*_ son
						*[#FFFFFF] Usados
							*_ para
								*[#FFFFFF] Referirse a los parámetros de entrada de las funciones.
					*_ el
						*[#FFFFFF] Almacenamiento de valores
							*_ es 
								*[#FFFFFF] Temporal.
							*_ su
								*[#FFFFFF] Alcance
									*_ se
										*[#FFFFFF] Limita al alcance de la función.
			*_ los
				*[#36C5FF] Bucles desaparecen
					*_ se 
						*[#FFFFFF] Sustituye por la recursión
							*_ es
								*[#FFFFFF] Aquella función que hace referencia así misma.
			*_ las 
				*[#36C5FF] Constantes
					*_ son
						*[#FFFFFF] Funciones que devuelven el mismo resultado.

	*_ basado en
		*[#079CA1] <font color=#FFFFFF> Lambda cálculo
			*_ presentado en
				*[#FFFFFF] en 1930
					*_ autores
						*[#FFFFFF] Alonzo Church
						*[#FFFFFF] Stephen Kleene
			*_ la idea era
				*[#FFFFFF] Un sistema
					*_ para estudiar
						*[#FFFFFF] El concepto de función.
						*[#FFFFFF] La aplicación de funciones de recursividad.
			*_ se obtuvo
				*[#FFFFFF] Un sistema computacional
					*_ era
						*[#FFFFFF] Muy potente
						*[#FFFFFF] Similar a von Neumann.
			*_ tiene
				*[#FFFFFF] Una variante
					*_ llamada
						*[#FFFFFF] Lógica combinatoria
							*_ creado
								*[#FFFFFF] En 1965
								*[#FFFFFF] Por Peter Landin
							*_ es
								*[#FFFFFF] Un modelo simplificado del cómputo.

	*_ tiene
		*[#079CA1] <font color=#FFFFFF> Conceptos
			*_ tipos de
				*[#FFFFFF] Evaluación
					*_ la
						*[#FFFFFF] Evaluación perezosa
							*_ solo
								*[#FFFFFF] Realiza los cálculos necesarios. 
							*_ evalua
								*[#FFFFFF] De fuera hacia dentro.
						*[#FFFFFF] Evaluación estrica - impaciente
							*_ usado en
								*[#FFFFFF] El paradigma imperativo.
							*_ evalua
								*[#FFFFFF] Adentro hacia afuera.
			*_ la
				*[#FFFFFF] Memoización
					*_ consiste en
						*[#FFFFFF] Almacenar el valor de la expresión que la evaluación ya haya sido realizada.
					*_ es 
						*[#FFFFFF] Un paso por necesidad.
@endmindmap
```

Recurso utilizado

[Video - Lenguaje de Programación Funcional](https://canal.uned.es/video/5a6f4bf3b1111f082a8b4708)